# Valorant Color Cheat
Valorant - The new CSGO/Overwatch Game from Riot Games.
Aimbot and Triggerbot programmed in Java. Detecting players on screen by color scanning & filter algorithms. 
Without access to memory → Nearly 100% Undetectable. 

The Program is under MIT License, you can do want you want with the source code. I dont give Liability or Warranty.

Used libraries:
- JNA 5.2.0
- JNA Platform 5.2.0
- JNativHook 2.1.0
- JavaFX 11.0.2


How to use:
1. Install Java 8 from https://www.java.com/
2. Download the Cheat from the release page
3. Go into Valorant -> Settings -> General -> Set Enemy Highlight Color to Purple
4. Start the Jar File -> Ready to use (A gui is opened)
5. Hold right click or fifth mouse button to use aimbot/triggerbot.


Tips:
- Do not set fov too high, the cheat will have a high reaction time. (Activate Debug to see your CPU time/tps, you should have over ~25 tps)
- Set max TPS not under your FPS Limit and not more as double of your average FPS.
- Click "Load Weapon Config" and select your weapon at weapon change, for optimal settings.
- Use Triggerbot only in combination with Aimbot.


It iS NoT wOrKiNg!!1!!?!
- Nothing is happening: https://lmgtfy.com/?q=jar+file+not+starting - Try to start it with CMD/Batch.
- Aimbot/Triggerbot not working: Read "How to use" or restart the cheat.
- The Triggerbot is not hitting: Check your settings or you are too dump to play with cheats. (Okay, the Trigger for Snipers are not perfect)
- Some bugs or errors? Write a ticket at Issues or contact my on Discord (NiroDev#7076).


Requirement:
- Java 8
- Windows 10 (7 and 8 may also work)
